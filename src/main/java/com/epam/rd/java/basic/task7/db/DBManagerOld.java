/*
package com.epam.rd.java.basic.task7.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;


public class DBManagerOld {
	private static final String URL = "jdbc:mysql://localhost:3306/test2db";
	private static final String USER = "root";
	private static final String PASSWORD = "123456";

	public static Connection con;
	private static DBManagerOld instance;

	public static synchronized DBManagerOld getInstance() {
		if(instance == null) {
			instance = new DBManagerOld();
		}
		return instance;
	}

	private DBManagerOld() {
		try {
			con = DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void setDemoConfiguration() throws DBException {
		Statement stmt;
		try {
			stmt = con.createStatement();
			stmt.addBatch(DBConfiguration.DROP_DATABASE);
			stmt.addBatch(DBConfiguration.CREATE_DATABASE);
			stmt.addBatch(DBConfiguration.USE_DATABASE);
			stmt.addBatch(DBConfiguration.CREATE_TABLE_USERS);
			stmt.addBatch(DBConfiguration.CREATE_TABLE_TEAMS);
			stmt.addBatch(DBConfiguration.CREATE_TABLE_USERS_TEAMS);
			stmt.addBatch(DBConfiguration.INSERT_USERS);
			stmt.addBatch(DBConfiguration.INSERT_TEAMS);
			stmt.executeBatch();
		} catch (SQLException e) {
			throw new DBException("exception while executing query", e);
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try (Statement stmt = con.createStatement();
			 ResultSet rs = stmt.executeQuery(DBConstants.SELECT_ALL_USERS)) {
			while (rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
				users.add(user);
			}
		} catch (SQLException e) {
			throw new DBException("exception while executing query", e);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		if(user == null) {
			return false;
		}
		ResultSet rs = null;
		try (PreparedStatement pstmt = con.prepareStatement(DBConstants.INSERT_USER,
					 Statement.RETURN_GENERATED_KEYS)) {
			pstmt.setString(1, user.getLogin());
			pstmt.execute();
			rs = pstmt.getGeneratedKeys();
			while (rs.next()) {
				user.setId(rs.getInt(1));
			}
		} catch (SQLException e) {
			throw new DBException("exception while executing query", e);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				throw new DBException("exception while executing query", e);
			}
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		PreparedStatement pstmt = null;
		try{
			pstmt = con.prepareStatement(DBConstants.DELETE_USER_1);
			for(User user : users) {
				if(user == null) {
					return false;
				}
				pstmt.setInt(1, user.getId());
				pstmt.execute();
			}
			pstmt = con.prepareStatement(DBConstants.DELETE_USER_2);
			for(User user : users) {
				pstmt.setInt(1, user.getId());
			}
		} catch (SQLException e) {
			throw new DBException("exception while executing query", e);
		} finally {
			if(pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					throw new DBException("exception while executing query", e);
				}
			}
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		User user = null;
		ResultSet rs = null;
		try (PreparedStatement pstmt = con.prepareStatement(DBConstants.GET_USER)) {
			pstmt.setString(1, login);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				user = new User();
				user.setId(rs.getInt("id"));
				user.setLogin(login);
			}
		} catch (SQLException e) {
			throw new DBException("exception while executing query", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					throw new DBException("exception while executing query", e);
				}
			}
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = null;
		ResultSet rs = null;
		try (PreparedStatement pstmt = con.prepareStatement(DBConstants.GET_TEAM)) {
			pstmt.setString(1, name);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				team = new Team();
				team.setId(rs.getInt("id"));
				team.setName(name);
			}
		} catch (SQLException e) {
			throw new DBException("exception while executing query", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					throw new DBException("exception while executing query", e);
				}
			}
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try (Statement stmt = con.createStatement();
			 ResultSet rs = stmt.executeQuery(DBConstants.SELECT_ALL_TEAMS)) {
			while (rs.next()) {
				Team team = new Team();
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
				teams.add(team);
			}
		} catch (SQLException e) {
			throw new DBException("exception while executing query", e);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		ResultSet rs = null;
		try(PreparedStatement pstmt = con.prepareStatement(DBConstants.INSERT_TEAM,
					 Statement.RETURN_GENERATED_KEYS)) {
			pstmt.setString(1, team.getName());
			pstmt.execute();
			rs = pstmt.getGeneratedKeys();
			while (rs.next()) {
				team.setId(rs.getInt(1));
			}
		} catch (SQLException e) {
			throw new DBException("exception while executing query", e);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				throw new DBException("exception while executing query", e);
			}
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		if(user == null) {
			return false;
		}
		PreparedStatement pstmt = null;
		try {
			con.setAutoCommit(false);
			pstmt = con.prepareStatement(DBConstants.INSERT_USER_TO_TEAM);
			for (Team team : teams) {
				pstmt.setInt(1, user.getId());
				pstmt.setInt(2, team.getId());
				pstmt.execute();
			}
			con.commit();
		} catch (SQLException e) {
			try {
				if (con != null) {
					con.rollback();
				}
			} catch (SQLException ex) {
				throw new DBException("exception while executing query", ex);
			}
			throw new DBException("exception while executing query", e);
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					throw new DBException("exception while executing query", e);
				}
			}
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement(DBConstants.GET_USER_TEAMS);
			pstmt.setInt(1, user.getId());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				teams.add(getTeam(rs.getString("name")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					throw new DBException("exception while executing query", e);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					throw new DBException("exception while executing query", e);
				}
			}
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		if(team == null) {
			return false;
		}
		PreparedStatement pstmt = null;
		try{
			pstmt = con.prepareStatement(DBConstants.DELETE_TEAM_1);
			pstmt.setInt(1, team.getId());
			pstmt.execute();
			pstmt = con.prepareStatement(DBConstants.DELETE_TEAM_2);
			pstmt.setInt(1, team.getId());
			pstmt.execute();
		} catch (SQLException e) {
			throw new DBException("exception while executing query", e);
		} finally {
			if(pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					throw new DBException("exception while executing query", e);
				}
			}
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		if(team == null) {
			return false;
		}
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement(DBConstants.UPDATE_TEAM);
			pstmt.setString(1, team.getName());
			pstmt.setInt(2, team.getId());
			pstmt.execute();
		} catch (SQLException e) {
			throw new DBException("exception while executing query", e);
		} finally {
			if(pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					throw new DBException("exception while executing query", e);
				}
			}
		}
		return true;
	}
}

*/
