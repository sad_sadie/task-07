package com.epam.rd.java.basic.task7.db;

public class DBConstants {
    public static String SELECT_ALL_USERS = "SELECT * FROM users";
    public static String SELECT_ALL_TEAMS = "SELECT * FROM teams";
    public static String GET_USER = "SELECT id FROM users WHERE login = ?";
    public static String GET_TEAM = "SELECT id FROM teams WHERE name = ?";
    public static String GET_USER_TEAMS =
            "SELECT name " +
            "FROM teams, users, users_teams " +
            "WHERE teams.id = users_teams.team_id " +
            "AND users_teams.user_id = users.id " +
            "AND users.id = ?;";
    public static String INSERT_USER = "INSERT INTO users(login) VALUES(?);";
    public static String INSERT_TEAM = "INSERT INTO teams(name) VALUES(?);";
    public static String INSERT_USER_TO_TEAM = "INSERT INTO users_teams VALUES(?, ?);";
    public static String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?;";
    public static String DELETE_USER_1 = "DELETE FROM users WHERE id = ?;";
    public static String DELETE_USER_2 = "DELETE FROM users_teams WHERE team_id = ?;";
    public static String DELETE_TEAM_1 = "DELETE FROM teams WHERE id = ?;";
    public static String DELETE_TEAM_2 = "DELETE FROM users_teams WHERE user_id = ?;";
}
