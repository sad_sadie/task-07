package com.epam.rd.java.basic.task7.db;

public class DBConfiguration {
    public static String DROP_DATABASE = "DROP database IF EXISTS test2db;";
    public static String CREATE_DATABASE = "CREATE database test2db;";
    public static String USE_DATABASE = "USE test2db;";
    public static String CREATE_TABLE_USERS =
            "CREATE TABLE users (\n" +
            "\tid INT PRIMARY KEY auto_increment,\n" +
            "\tlogin VARCHAR(10) UNIQUE\n" +
            ");";
    public static String CREATE_TABLE_TEAMS =
            "CREATE TABLE teams (\n" +
            "\tid INT PRIMARY KEY auto_increment,\n" +
            "\tname VARCHAR(10)\n" +
            ");";
    public static String CREATE_TABLE_USERS_TEAMS =
            "CREATE TABLE users_teams (\n" +
            "\tuser_id INT REFERENCES users(id) on delete cascade,\n" +
            "\tteam_id INT REFERENCES teams(id) on delete cascade,\n" +
            "\tUNIQUE (user_id, team_id)\n" +
            ");";
    public static String INSERT_USERS = "INSERT INTO users VALUES (DEFAULT, 'ivanov');";
    public static String INSERT_TEAMS = "INSERT INTO teams VALUES (DEFAULT, 'teamA');";
}
